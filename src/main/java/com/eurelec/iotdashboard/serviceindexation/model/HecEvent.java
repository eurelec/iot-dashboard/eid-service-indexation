package com.eurelec.iotdashboard.serviceindexation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class HecEvent {
    Object event;
    @Value("${splunk.iotHeartbeatCollector.index}")
    String index;
}
