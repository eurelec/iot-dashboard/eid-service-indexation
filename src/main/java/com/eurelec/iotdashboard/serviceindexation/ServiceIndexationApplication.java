package com.eurelec.iotdashboard.serviceindexation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients(basePackages = {"com.eurelec.iotdashboard.serviceindexation.client"})
@SpringBootApplication
public class ServiceIndexationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceIndexationApplication.class, args);
    }

}
