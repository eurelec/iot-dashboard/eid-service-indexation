package com.eurelec.iotdashboard.serviceindexation.interceptor;

import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SplunkHecInterceptor {
    @Value("${splunk.iotHeartbeatCollector.token}")
    String token;
    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("Authorization", token);
        };
    }
}
