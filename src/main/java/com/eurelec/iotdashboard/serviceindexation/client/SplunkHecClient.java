package com.eurelec.iotdashboard.serviceindexation.client;

import com.eurelec.iotdashboard.serviceindexation.model.HecEvent;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "SplunkHecClient", url = "http://localhost:8088/services/collector")
public interface SplunkHecClient {

    @PostMapping
    Object postEvent(@RequestBody HecEvent paylaod);

}
