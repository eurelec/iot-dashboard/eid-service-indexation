package com.eurelec.iotdashboard.serviceindexation.consumer;

import com.eurelec.iotdashboard.eidserviceecoute.Payload;
import com.eurelec.iotdashboard.serviceindexation.client.SplunkHecClient;
import com.eurelec.iotdashboard.serviceindexation.model.HecEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class IoTHeartbeatConsumer {

    @Value("${splunk.iotHeartbeatCollector.index}")
    String index;

    private final SplunkHecClient splunkHecClient;

    @KafkaListener(
            topics = "${kafka.topics.iotHeartbeat}",
            containerFactory = "IoTHeartbeatKafkaListenerContainerFactory")
    public void iotHeartbeatListener(Payload heartbeat) {
        splunkHecClient.postEvent(new HecEvent(heartbeat.getBody(), index));

    }
}
