package com.eurelec.iotdashboard.eidserviceecoute;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Payload {
    String id;
    String secret;
    Object body;
}
